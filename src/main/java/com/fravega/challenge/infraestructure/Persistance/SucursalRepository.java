package com.fravega.challenge.infraestructure.Persistance;

import com.fravega.challenge.domain.sucursal.repositories.SucursalDomainRepository;
import com.fravega.challenge.domain.sucursal.Sucursal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
public interface SucursalRepository extends JpaRepository<Sucursal, Long>, SucursalDomainRepository {
   
    
}
