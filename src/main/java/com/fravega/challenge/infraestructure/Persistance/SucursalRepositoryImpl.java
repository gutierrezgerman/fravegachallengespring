/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.infraestructure.Persistance;
import com.fravega.challenge.domain.sucursal.Sucursal;
import com.fravega.challenge.domain.sucursal.repositories.SucursalDomainRepository;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.spi.LocationAwareLogger;
import org.springframework.stereotype.Repository;

@Repository
public class SucursalRepositoryImpl  implements SucursalDomainRepository  {
    
    @PersistenceContext
	private EntityManager entityManager;

    
    
    @Override
    public boolean checkHealh() {
        try{
        Object item = (Object)  entityManager.createNativeQuery("select 1 from dual")
                .setHint("javax.persistence.query.timeout", 1000)
                .getSingleResult();
        
        return item.equals(1);
        } catch(org.springframework.dao.DataAccessResourceFailureException e){
            
            return false;
        } 
        catch(PersistenceException e){
            
            return false;
        } 
        
    }

    @Override
    public Sucursal getNear(Double longitud, Double latitud) {
        
        

        Query q = entityManager.createNativeQuery(
            "select id, direccion, X(location) as longitud, "
                        + "Y(location) as latitud , "
                        + "ST_Distance(location, Point(?,?)) as distancia "
                        + "from sucursales order by distancia asc limit 1");
        q.setParameter(1,longitud);
        q.setParameter(2,latitud);
        Object[] item = (Object[]) q.getSingleResult();
        
        
        Long longid =  Long.valueOf(item[0].toString());
        
        System.out.println("getnear()");
        return new Sucursal( 
            longid,
            (String) item[1], 
            (Double) item[2], 
            (Double) item[3]
        );
        
    }

    @Override
   public void deleteSucursales(){
      EntityManager em  = entityManager.getEntityManagerFactory().createEntityManager();
       em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM sucursales").executeUpdate();
 System.out.println("deleteSucursales()");
       em.getTransaction().commit();
          
          
         
   }
    @Override
    public Sucursal findSucursal(Long id) {
        
        Query q = entityManager.createNativeQuery(
            "SELECT id, direccion, "
            + "X(location) as longitud, "
            + "Y(location) as latitud FROM sucursales s where s.id = ?");
        q.setParameter(1,id);
        Object[] item = (Object[]) q.getSingleResult();
        
        
        
        
        
        Long longid =  Long.valueOf(item[0].toString());
        
        
        
        return new Sucursal( 
            longid,
            (String) item[1], 
            (Double) item[2], 
            (Double) item[3]
        );
    }

    

    @Override
    public List<Sucursal> findAllSucursales() {
        List<Object[]> items = entityManager.createNativeQuery(
            "SELECT id, direccion, X(location) as longitud, Y(location) as latitud FROM sucursales" ).getResultList();
        List<Sucursal> sucursales = new ArrayList<Sucursal>();
        for(Object[] item : items) {
            //System.out.println(item[0] +" = "+ item[1]);
            Long longid =  Long.valueOf(item[0].toString());
            sucursales.add(
                new Sucursal( 
                    longid,
                    (String) item[1], 
                    (Double) item[2], 
                    (Double) item[3]
                )
            );
        }
        return sucursales;
    }

    /**
     *
     * @param direccion
     * @param longitud
     * @param latitud
     */
    @Transactional
    public Sucursal createSucursal(String direccion, Double longitud, Double latitud) {
        
          Integer id =  entityManager.createNativeQuery("INSERT INTO sucursales ( direccion, location, created_at)"
                    + " VALUES ( :direccion,POINT( :longitud, :latitud), :fecha)")
                    .setParameter("direccion", direccion)
                    .setParameter("longitud", longitud)
                    .setParameter("latitud", latitud)
                    .setParameter("fecha", new java.util.Date()).executeUpdate();
          
          entityManager.flush();
         
          
        return new Sucursal(direccion,longitud, latitud );
    }

  


    
}
