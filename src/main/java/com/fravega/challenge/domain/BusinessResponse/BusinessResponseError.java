/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.domain.BusinessResponse;

import com.fravega.challenge.domain.sucursal.Sucursal;
import java.util.List;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
public class BusinessResponseError {
    
    public HttpStatus statusCode;
    public Sucursal data;

    public BusinessResponseError(Sucursal data, HttpStatus statusCode) {
        this.statusCode = statusCode;
        this.data = data;
    }
    
    
}
