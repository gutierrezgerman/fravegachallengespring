/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.domain.sucursal.exceptions;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
public class SucursalDataIntegrityViolationException extends RuntimeException{
  public SucursalDataIntegrityViolationException(String msg){
    super(msg);
  }
  @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
