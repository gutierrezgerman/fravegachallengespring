package com.fravega.challenge.application.handlers;

import com.fravega.challenge.domain.sucursal.exceptions.SucursalDataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.fravega.challenge.domain.sucursal.exceptions.SucursalNotFoundException;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
@RestControllerAdvice
public class ErrorHandler {
 
    @ExceptionHandler(SucursalNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public SucursalNotFoundException handleCustomException(SucursalNotFoundException ce) {
        return ce;
    }
    
    @ExceptionHandler(SucursalDataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public SucursalDataIntegrityViolationException handleCustomException(SucursalDataIntegrityViolationException ce) {
        return ce;
    }
 
}
