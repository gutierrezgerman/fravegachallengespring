/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.application.controllers;

import com.fravega.challenge.domain.BusinessRequest.SucursalBusinessRequest;
import com.fravega.challenge.domain.BusinessResponse.BusinessResponse;
import com.fravega.challenge.domain.sucursal.Sucursal;
import com.fravega.challenge.domain.sucursal.services.SucursalService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fravega.challenge.ChallengeApplication;
import com.fravega.challenge.infraestructure.Persistance.SucursalRepository;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import org.junit.After;
import org.junit.Before;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengeApplication.class)
@AutoConfigureMockMvc
public class SucursalControllerTest
{
	@Autowired
	private MockMvc mvc;
        
  
        @Test
	void contextLoads() {
	}
        
        

	@Test
	public void testCreateSucursalCongreso() throws Exception
	{

		SucursalBusinessRequest sucursalBr = new SucursalBusinessRequest();
		sucursalBr.direccion= "Congreso";
		sucursalBr.longitud = -34.6103181;
                sucursalBr.latitud = -58.3899012;
		
		mvc.perform(
				post("/sucursal").contentType(MediaType.APPLICATION_JSON).content(toJson(sucursalBr)))
				.andExpect(status().isOk());
                
        }     
        
        
        
        @Test
	public void testCreateSucursalCongresoError() throws Exception
	{
            
            
            SucursalBusinessRequest sucursalBr = new SucursalBusinessRequest();
            sucursalBr.direccion= "Congreso";
            sucursalBr.longitud = -34.6103181;
            sucursalBr.latitud = -58.3899012;

            mvc.perform(
                    post("/sucursal").contentType(MediaType.APPLICATION_JSON).content(toJson(sucursalBr)))
                        .andExpect(status().is5xxServerError());
	}
        
	              
        @Test
        public void testCreateSucursalObelisco() throws Exception
	{
		SucursalBusinessRequest obelisco = new SucursalBusinessRequest();
                obelisco.direccion= "Obelisco";
                obelisco.longitud = -34.6041626;
                obelisco.latitud = -58.3821766;
		

		mvc.perform(
				post("/sucursal").contentType(MediaType.APPLICATION_JSON).content(toJson(obelisco)))
				.andExpect(status().isOk());
                
        }          
        
        
        @Test
         public void testCreateSucursalPdeMayo() throws Exception
	{
		SucursalBusinessRequest mayo = new SucursalBusinessRequest();
                mayo.direccion= "Plaza de Mayo";
                mayo.longitud = -34.6101979;
                mayo.latitud = -58.375697;
		
		mvc.perform(
				post("/sucursal").contentType(MediaType.APPLICATION_JSON).content(toJson(mayo)))
				.andExpect(status().isOk());
                
        }  
        

	

	@Test
	public void testGetnear() throws Exception
	{
            
               
		SucursalBusinessRequest sucursalBr = new SucursalBusinessRequest();
		// "Catedral Linea D";
		sucursalBr.longitud = -34.6081594;
                sucursalBr.latitud = -58.3743031;

		mvc.perform(post("/sucursal/getnear").contentType(MediaType.APPLICATION_JSON).content(toJson(sucursalBr)))
				.andExpect(status().isOk())
                                .andExpect((jsonPath("$.data.direccion", is("Plaza de Mayo"))));
 
		
	}

        @Test void testDelete() throws Exception {
            
            mvc.perform(delete("/sucursal/delete")).andReturn();
        }
	private byte[] toJson(Object obj) throws IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return mapper.writeValueAsBytes(obj);
	}

    


}
