FROM maven:3-openjdk-11
ARG PORT=5000
ARG WORKDIR=/usr/src/app
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
WORKDIR  ${WORKDIR}


EXPOSE 9000
EXPOSE 8080
#EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["bash"]

